#!/usr/bin/env iocsh.bash

# -----------------------------------------------------------------------------
# EPICS - DataBase
# -----------------------------------------------------------------------------
# MCU # 006
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI and MCA groups
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# MCAG - torsten.bogershausen@esss.se
# -----------------------------------------------------------------------------
require EthercatMC, 3.0.1

# -----------------------------------------------------------------------------
# IOC common settings
# -----------------------------------------------------------------------------
epicsEnvSet("MOTOR_PORT",    "$(SM_MOTOR_PORT=MCU1)")
epicsEnvSet("IPADDR",        "$(SM_IPADDR=172.30.242.17)")
epicsEnvSet("IPPORT",        "$(SM_IPPORT=200)")
epicsEnvSet("ASYN_PORT",     "$(SM_ASYN_PORT=MC_CPU1)")
epicsEnvSet("PREFIX",        "$(SM_PREFIX=LabS-ESSIIP:)")
epicsEnvSet("P",             "$(SM_PREFIX=LabS-ESSIIP:)")
epicsEnvSet("EGU",           "$(SM_EGU=mm)")
epicsEnvSet("PREC",          "$(SM_PREC=3)")

# -----------------------------------------------------------------------------
# EtherCAT MC Controller
# -----------------------------------------------------------------------------
iocshLoad("$(EthercatMC_DIR)EthercatMCController.iocsh")


# -----------------------------------------------------------------------------
# Axis 1 configuration and instantiation
# -----------------------------------------------------------------------------
epicsEnvSet("MOTOR_NAME",    "$(SM_MOTOR_NAME=MC-MCU-01:m1)")
epicsEnvSet("AXIS_NO",       "$(SM_AXIS_NO=1)")
epicsEnvSet("DESC",          "$(SM_DESC=Lower=Right)")
epicsEnvSet("AXISCONFIG",    "adsport=852;HomProc=1;HomPos=0.0;encoder=ADSPORT=501/.ADR.16#3040040,16#8000001C,2,2")

iocshLoad("$(EthercatMC_DIR)EthercatMCAxis.iocsh")
iocshLoad("$(EthercatMC_DIR)EthercatMCAxisdebug.iocsh")
iocshLoad("$(EthercatMC_DIR)EthercatMCAxishome.iocsh")

